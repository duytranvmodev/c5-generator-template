# generator-template

## Installation

Install [yeoman-generator](http://yeoman.io/)

```bash
npm install -g yo
```

We will be using this generator as local generator (this generator will not be availabe in npmjs yet). Clone this repo in your local disk and run `npm link` to create a symlink to this generator.

```bash
cd ~/generator-template
npm install
npm link
```

Then generate your new project:

```bash
yo c5-template

// to skip running npm install
yo c5-template --skip-install
```