'use strict';
const path = require('path');
const assert = require('yeoman-assert');
const helpers = require('yeoman-test');

const APP_NAME = 'api-project';
const APP_DESCRIPTION = 'I heard you like generators';
const APP_VERSION = '1.0.0';
const SERVER_PORT = 6969;
const NODE_VERSION = '9.9.9';
const APP_GROUP = 'api-test';

describe('generator-template:api', () => {
  beforeAll(() =>
    helpers.run(path.join(__dirname, '../generators/api')).withPrompts({
      name: APP_NAME,
      serverPort: SERVER_PORT,
      appVersion: APP_VERSION,
      description: APP_DESCRIPTION,
      nodeVersion: NODE_VERSION,
      group: APP_GROUP,
    }),
  );

  it('created folder named like the generator', () => {
    assert.file(path.join(process.cwd(), APP_NAME));
  });

  it('creates files', () => {
    const generatedFoldersFiles = [
      'config',
      'src',
      'test',
      'package.json',
      '.eslintignore',
      '.eslintrc.js',
      '.gitignore',
      '.gitlab-ci.yml',
      '.nvmrc',
      '.prettierignore',
      '.prettierrc',
      'nest-cli.json',
      'nodemon.json',
      'nodemon-debug.json',
      'README.md',
      'swagger.json',
      'swagger-2.0.json',
      'tsconfig.json',
      'tsconfig.build.json',
      'Dockerfile',
      '.dockerignore',
    ];

    assert.file(generatedFoldersFiles.map((p) => path.join(process.cwd(), APP_NAME, `${p}`)));
  });

  describe('when setting server port number (for local development)', () => {
    it('config file should be set in config/default.yaml file', () => {
      const defaultConfigPath = path.join(process.cwd(), APP_NAME, 'config/default.yaml');
      assert.fileContent(defaultConfigPath, `port: ${SERVER_PORT}`);
      assert.fileContent(defaultConfigPath, `hostname: 127.0.0.1:${SERVER_PORT}`);
    });
    it('should set swagger host port', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, 'swagger.json'),
        `"url": "http://127.0.0.1:${SERVER_PORT}"`,
      );
    });
    it('should set swagger-2.0 host port', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, 'swagger-2.0.json'),
        `"host": "127.0.0.1:${SERVER_PORT}"`,
      );
    });
  });

  describe('when setting node version', () => {
    it('node version should be set in nvmrc file', () => {
      assert.fileContent(path.join(process.cwd(), APP_NAME, '.nvmrc'), `v${NODE_VERSION}`);
    });
    it('base node version in Dockerfile file should be set', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, 'Dockerfile'),
        `FROM node:${NODE_VERSION}-alpine AS build`,
      );
    });
    it('base node version in .gitlab-ci.yml file should be set', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, '.gitlab-ci.yml'),
        `DOCKER_IMAGE_NAME: node:${NODE_VERSION}`,
      );
    });
  });

  describe('when creating package.json file', () => {
    it('name should be set', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, 'package.json'),
        `"name": "${APP_NAME}"`,
      );
    });
    it('description should be set', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, 'package.json'),
        `"description": "${APP_DESCRIPTION}"`,
      );
    });
    it('author should be set', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, 'package.json'),
        `"author": "tech@c5.my"`,
      );
    });
    it('private should be set to true', () => {
      assert.fileContent(path.join(process.cwd(), APP_NAME, 'package.json'), `"private": true`);
    });
    it('license should be set', () => {
      assert.fileContent(path.join(process.cwd(), APP_NAME, 'package.json'), `"license": "MIT"`);
    });
    it('version should be set', () => {
      assert.fileContent(
        path.join(process.cwd(), APP_NAME, 'package.json'),
        `"version": "${APP_VERSION}"`,
      );
    });
  });

  describe('when creating README.md file', () => {
    it('name should be set', () => {
      assert.fileContent(path.join(process.cwd(), APP_NAME, 'README.md'), `# ${APP_NAME}`);
    });
  });

  const appTestCases = [
    ['api-project', '/api/project', '/docs/project'],
    ['api-my-project', '/api/my-project', '/docs/my-project'],
    ['my-other-project', '/api/my-other-project', '/docs/my-other-project'],
    ['api-api-project', '/api/api-project', '/docs/api-project'],
    ['api-myapi-project', '/api/myapi-project', '/docs/myapi-project'],
  ];

  const runGenerator = async (appName) =>
    helpers.run(path.join(__dirname, '../generators/api')).withPrompts({
      name: appName,
      serverPort: SERVER_PORT,
      appVersion: APP_VERSION,
      description: APP_DESCRIPTION,
      nodeVersion: NODE_VERSION,
      group: APP_GROUP,
    });

  describe.each(appTestCases)(
    'when setting the group and name for %s',
    (appName, appApiBaseUrl, appDocsBaseUrl) => {
      beforeAll(() => runGenerator(appName));

      it('should set config/custom-environment-variables.yaml file', () => {
        assert.fileContent(
          path.join(process.cwd(), appName, 'config', 'custom-environment-variables.yaml'),
          `baseUrl: ${appApiBaseUrl}`,
        );
        assert.fileContent(
          path.join(process.cwd(), appName, 'config', 'custom-environment-variables.yaml'),
          `docsBaseUrl: ${appDocsBaseUrl}`,
        );
      });
      it('should set config/default.yaml file', () => {
        assert.fileContent(
          path.join(process.cwd(), appName, 'config', 'default.yaml'),
          `baseUrl: ${appApiBaseUrl}`,
        );
        assert.fileContent(
          path.join(process.cwd(), appName, 'config', 'default.yaml'),
          `docsBaseUrl: ${appDocsBaseUrl}`,
        );
      });
      it('should set swagger-2.0 file', () => {
        assert.fileContent(
          path.join(process.cwd(), appName, 'swagger-2.0.json'),
          `"${appApiBaseUrl}/ping"`,
        );
      });
      it('should set swagger file', () => {
        assert.fileContent(
          path.join(process.cwd(), appName, 'swagger.json'),
          `"${appApiBaseUrl}/ping"`,
        );
      });
    },
  );
});
