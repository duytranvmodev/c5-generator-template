module.exports = [
  {
    type: 'input',
    name: 'group',
    message: 'Enter gitlab group',
    default: 'vmo/services',
    validate(group) {
      const validGroups = ['vmo/services'];
      return validGroups.includes(group) || `Group must be one of ${validGroups}`;
    },
  },
  {
    type: 'input',
    name: 'name',
    message: "Enter your app's name",
    default: 'api-beautiful',
    validate(name) {
      return /^[a-z\\-]*$/.test(name) || 'App name can only contain lowercase letters and -';
    },
  },
  {
    type: 'input',
    name: 'serverPort',
    message: `Server port`,
    default: 3000,
  },
  {
    type: 'input',
    name: 'appVersion',
    message: 'Enter version of the app',
    default: '0.1.0',
  },
  {
    type: 'input',
    name: 'description',
    message: 'Enter a description for your app.',
    default: '',
  },
  {
    type: 'input',
    name: 'nodeVersion',
    message: "Enter your app's node version",
    default: '16.13.0',
    validate(name) {
      const validNodeVersions = [
        '16.13.0', // Lts/gallium
        '14.15.2', // Lts/fermium
        '12.20.0', // Lts/erbium
        '14.18.0',
      ];
      return (
        validNodeVersions.includes(name) ||
        `Node version must be one of ${validNodeVersions} versions`
      );
    },
  },
  {
    type: 'input',
    name: 'isIncludeNewRelic',
    message: "Include New Relic? (y/n)",
    default: 'n',
    validate(name) {
      const validAnswer = [
        'y',
        'n',
      ];
      return (
        validAnswer.includes(name) ||
        `You should be choices one of ${validAnswer}`
      );
    },
  },
  {
    type: 'input',
    name: 'isIncludesSonar',
    message: "Include Sonar? (y/n)",
    default: 'n',
    validate(name) {
      const validAnswer = [
        'y',
        'n',
      ];
      return (
        validAnswer.includes(name) ||
        `You should be choices one of ${validAnswer}`
      );
    },
  },
  {
    type: 'input',
    name: 'kindOfFramework',
    message: "Which framework do you would like? (Strapi/NestJs)",
    default: 'NestJs',
    validate(name) {
      const validAnswer = [
        'NestJs',
        'Strapi',
      ];
      return (
        validAnswer.includes(name) ||
        `You should be choices one of ${validAnswer}`
      );
    },
  },
];
