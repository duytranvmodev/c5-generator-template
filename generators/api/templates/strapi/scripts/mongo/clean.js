#!/usr/bin/env zx

require('../zx');

void (async () => {
  const { docker } = argv;
  if (docker) {
    await $`docker-compose exec -T mongo sh -c "mongo <%= config.name %> --username=<%= config.name %> --password=<%= config.name %> --authenticationDatabase admin --eval 'db.dropDatabase()'"`;
  } else {
    await $`mongo <%= config.name %> --eval 'db.dropDatabase()'`;
  }
})();
