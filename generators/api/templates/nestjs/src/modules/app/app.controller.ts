import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('ping')
  @ApiOperation({
    operationId: 'ping',
    description: 'Endpoint to test',
  })
  @ApiTags('examples')
  getHello(): string {
    return this.appService.getHello();
  }
}
