import * as pino from 'pino';
import { Module } from '@nestjs/common';
import { LoggerModule } from 'nestjs-pino';
import { MongooseModule } from '@nestjs/mongoose';
import { APP_FILTER } from '@nestjs/core';
import { ConfigModule } from '../config/config.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModuleConfigService } from '../../shared/mongo.helper';
import I18nConfigModule from '../../configs/i18n';
import { SampleMongoModule } from '../sample-mongo/sample-mongo.module';
import { AnyExceptionFilter, HttpExceptionFilter } from '../../filters';

@Module({
  imports: [
    ConfigModule,
    LoggerModule.forRootAsync({
      useFactory: async () => {
        return {
          pinoHttp: {
            level: process.env.NODE_ENV !== 'production' ? 'debug' : 'info',
            redact: {
              paths: ['req.headers["access-token"]'],
              censor: '***',
            },
            serializers: {
              err: pino.stdSerializers.err,
              req: pino.stdSerializers.req,
            },
            autoLogging: true,
          },
        };
      },
    }),
    MongooseModule.forRootAsync({ useClass: MongooseModuleConfigService }),
    I18nConfigModule,
    SampleMongoModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
  ],
})
export class AppModule {}
