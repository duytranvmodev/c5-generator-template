import { HttpStatus } from '@nestjs/common';

export interface IError {
  message: string;
  errorCode: string;
  description: string;
  statusCode: HttpStatus;
  stackTrace?: any;
}
