import { Document } from 'mongoose';
import { SampleMongoDto } from './dto/sample-mongo.dto';

export interface SampleMongoModel extends Document, SampleMongoDto {}
