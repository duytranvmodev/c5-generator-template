import { Schema, Document } from 'mongoose';
import * as mongoosePaginate from 'mongoose-paginate-v2';
import { ISampleMongo } from '../interfaces/sample-mongo.interface';

export const SampleMongoSchema = new Schema<ISampleMongo>({
  name: { type: String, required: true },
  value: { type: Number, required: true },
});

SampleMongoSchema.plugin(mongoosePaginate);

export const SAMPLE_MONGO_SCHEMA_NAME = 'SampleSchema';
export type SampleMongoDocument = ISampleMongo & Document;
