import { Test, TestingModule } from '@nestjs/testing';
import { SampleMongoController } from './sample-mongo.controller';
import { SampleMongoService } from './sample-mongo.service';

describe('SampleMongoController', () => {
  let controller: SampleMongoController;
  let mockService = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SampleMongoController],
      providers: [{ provide: SampleMongoService, useValue: mockService }],
    }).compile();

    controller = module.get<SampleMongoController>(SampleMongoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
