import { PartialType } from '@nestjs/mapped-types';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsNumber } from 'class-validator';

export class CreateSampleMongoDto {
  @ApiProperty({})
  @IsString()
  @IsNotEmpty()
  name: string;

  @ApiProperty({})
  @IsNumber()
  @IsNotEmpty()
  value: number;
}
export class UpdateSampleMongoDto extends PartialType(CreateSampleMongoDto) {}
export class SampleMongoDto extends CreateSampleMongoDto {}