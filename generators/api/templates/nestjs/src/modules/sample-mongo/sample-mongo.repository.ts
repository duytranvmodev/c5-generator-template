import { InjectModel } from '@nestjs/mongoose';
import { BaseRepository } from '../../base/base.repository';
import { SampleMongoModel } from './sample-mongo.model';
import { SAMPLE_MONGO_SCHEMA_NAME } from './schemas/sample-mongo.schema';

export class SampleMongoRepository extends BaseRepository<SampleMongoModel> {
  constructor(@InjectModel(SAMPLE_MONGO_SCHEMA_NAME) model) {
    super(model);
  }
}
