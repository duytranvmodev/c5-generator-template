import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpCode,
  UseInterceptors,
  Query,
} from '@nestjs/common';
import { I18n, I18nContext } from 'nestjs-i18n';
import { ApiOperation, ApiTags, ApiBearerAuth, ApiQuery } from '@nestjs/swagger';

import { BaseTransferInterceptor } from '../interceptors/response.interceptor';
import { CreateSampleMongoDto, UpdateSampleMongoDto } from './dto/sample-mongo.dto';
import { SampleMongoService } from './sample-mongo.service';
import { BaseRequestDto } from '../../base/base.request.dto';

@ApiTags('mongo')
@Controller('sample-mongo')
@UseInterceptors(BaseTransferInterceptor)
@ApiBearerAuth()
export class SampleMongoController {
  constructor(private readonly sampleMongoService: SampleMongoService) {}

  @ApiOperation({
    operationId: 'sampleMongoCreate',
    description: 'Create a sample mongo doc',
  })
  @Post()
  async create(@Body() createSampleMongoDto: CreateSampleMongoDto, @I18n() i18n: I18nContext) {
    await this.sampleMongoService.create(createSampleMongoDto);
    // Return a translated massage
    return {
      message: await i18n.t('errors.TEST'),
    };
  }

  @ApiOperation({
    operationId: 'getSampleMongoIndex',
    description: 'Get all sample mongo doc',
  })
  @Get()
  findAll() {
    return this.sampleMongoService.findAll();
  }

  @ApiOperation({
    operationId: 'getSampleMongoIndexAsPagiante',
    description: 'Get all sample mongo doc',
  })
  @ApiQuery({ name: 'page', required: false, example: 1 })
  @ApiQuery({ name: 'perPage', required: false, example: 10 })
  @Get('pagination')
  paginateSampleMongoDocs(@Query() query: BaseRequestDto) {
    return this.sampleMongoService.paginateSampleMongoDocs(query);
  }

  @ApiOperation({
    operationId: 'getSampleMongoById',
    description: 'Get a single sample mongo doc by id',
  })
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.sampleMongoService.findOne(id);
  }

  @ApiOperation({
    operationId: 'updateSampleMongoById',
    description: 'Update a single sample mongo doc by id',
  })
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSampleMongoDto: UpdateSampleMongoDto) {
    return this.sampleMongoService.update(id, updateSampleMongoDto);
  }

  @ApiOperation({
    operationId: 'deleteSampleMongoById',
    description: 'Delete a single sample mongo doc by id',
  })
  @HttpCode(204)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.sampleMongoService.remove(id);
  }
}
