import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';
import { SampleMongoService } from './sample-mongo.service';
import { SampleMongoDocument, SampleMongoSchema, SAMPLE_MONGO_SCHEMA_NAME } from './schemas/sample-mongo.schema';
import { SampleMongoRepository } from './sample-mongo.repository';
import { MongooseModuleConfigService } from '../../shared/mongo.helper';

describe('SampleMongoService', () => {
  let service: SampleMongoService;
  let sampleModel: Model<SampleMongoDocument>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRootAsync({useClass: MongooseModuleConfigService}),
        MongooseModule.forFeature(
          [
            { name: SAMPLE_MONGO_SCHEMA_NAME, schema: SampleMongoSchema }
          ]
        ),
      ],
      providers: [
        SampleMongoService,
        SampleMongoRepository,
      ],
    }).compile();

    service = module.get<SampleMongoService>(SampleMongoService);
    sampleModel = module.get(getModelToken(SAMPLE_MONGO_SCHEMA_NAME));
    await sampleModel.deleteMany({}).exec();
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
