import { Injectable } from '@nestjs/common';
import { CreateSampleMongoDto, UpdateSampleMongoDto } from './dto/sample-mongo.dto';
import { SampleMongoRepository } from './sample-mongo.repository';

@Injectable()
export class SampleMongoService {
  constructor(
    private readonly sampleRepository: SampleMongoRepository,
  ) {}

  create(createSampleMongoDto: CreateSampleMongoDto) {
    return this.sampleRepository.create(createSampleMongoDto);
  }

  findAll() {
    return this.sampleRepository
      .find();
  }

  findOne(id: string) {
    return this.sampleRepository
      .findById(id);
  }

  update(id: string, updateSampleMongoDto: UpdateSampleMongoDto) {
    return this.sampleRepository
      .findByIdAndUpdate(id, updateSampleMongoDto, { new: true });
  }

  remove(id: string) {
    return this.sampleRepository.findByIdAndRemove(id);
  }

  paginateSampleMongoDocs(query: Record<string, any>) {
    return this.sampleRepository.paginate({}, query);
  }
}
