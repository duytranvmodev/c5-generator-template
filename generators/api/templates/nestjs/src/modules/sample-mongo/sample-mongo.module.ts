import { Module } from '@nestjs/common';
import { SampleMongoService } from './sample-mongo.service';
import { SampleMongoController } from './sample-mongo.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { SAMPLE_MONGO_SCHEMA_NAME, SampleMongoSchema } from './schemas/sample-mongo.schema';
import { SampleMongoRepository } from './sample-mongo.repository';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: SAMPLE_MONGO_SCHEMA_NAME, schema: SampleMongoSchema }]),
  ],
  controllers: [SampleMongoController],
  providers: [
    SampleMongoService,
    SampleMongoRepository,
  ],
})
export class SampleMongoModule {}
