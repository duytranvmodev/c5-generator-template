import { Document, PaginateOptions, PaginateResult, PaginateModel } from 'mongoose';
import { BadRequestException, NotFoundException } from '@nestjs/common';
import { ModelNotFoundException } from '../modules/exceptions/model-not-found.exception';
import { EventEmitter } from 'events';

export class BaseRepository<T extends Document> extends EventEmitter {
  protected primaryKey = '_id';

  constructor(protected readonly model: PaginateModel<T>) {
    super();
    this.model = model;
  }
  // eslint-disable-next-line @typescript-eslint/ban-types
  async create(entity: object): Promise<T> {
    return new this.model(entity).save();
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  async createMany(entities: object[]): Promise<T[]> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return this.model.create(entities);
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  async createOrUpdate(entity: object): Promise<T> {
    let model = await this.findOne({
      [this.primaryKey]: entity[this.primaryKey],
    });

    if (model === null) {
      model = await new this.model(entity).save();
    } else {
      await model.set(entity).save();
    }

    return model;
  }

  async findById(id: string, populates: string[] = []): Promise<T> {
    const model = await this.model.findById(id);

    if (model && populates.length) {
      for (const path of populates) {
        await model.populate(path);
      }
    }

    if (!model) {
      throw new NotFoundException(`Not found id: ${id}`);
    }
    return model;
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  async findOne(params: object, populates: string[] = []): Promise<T> {
    const model = await this.model.findOne(params);

    if (model && populates.length) {
      for (const path of populates) {
        await model.populate(path);
      }
    }

    return model;
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  async find(params: object = {}, populates: string[] = []): Promise<T[]> {
    const models = await this.model.find(params);

    if (populates.length) {
      for (const path of populates) {
        for (const model of models) {
          await model.populate(path);
        }
      }
    }

    return models;
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  async findOneOrFail(params: object): Promise<T> {
    const model: T = await this.findOne(params);

    if (model === null) {
      throw new ModelNotFoundException(
        `Model [${this.getModel().collection.name}] not found for query ${JSON.stringify(params)}`,
      );
    }

    return model;
  }

  async findOrFail(id: string): Promise<T> {
    try {
      return await this.findById(id);
    } catch (e) {
      if (e.name !== undefined && e.name === 'CastError') {
        throw new BadRequestException(e.message);
      }

      throw e;
    }
  }

  // eslint-disable-next-line @typescript-eslint/ban-types
  async paginate(query: object, options: PaginateOptions): Promise<PaginateResult<T>> {
    const paging = await this.model.paginate(query, options);
    return paging;
  }

  async findAll(
    filter: { [key: string]: any } = {},
    options: { [key: string]: any } = {},
    limit = 0,
    sort: { [key: string]: any } = {},
  ): Promise<Array<T>> {
    const query = this.model
      .find(filter as any, null, options)
      .limit(limit)
      .sort(sort);
    return query.exec();
  }

  getModel(): PaginateModel<T> {
    return this.model;
  }

  async removeAll(filter: { [key: string]: any } = {}) {
    return this.model.deleteMany(filter as any);
  }

  async findByIdAndUpdate(id: string, entity: object, options: { [key: string]: any}) {
    return this.model.findByIdAndUpdate(id, entity, options);
  }

  async findByIdAndRemove(id: string) {
    return this.model.findByIdAndRemove(id);
  }
}
