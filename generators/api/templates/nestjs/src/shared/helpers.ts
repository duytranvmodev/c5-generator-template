import * as _ from 'lodash';
import {BaseResponse} from '../base/base.response';
import {IPaginationMeta, IPagination} from './interfaces';
import {
  TOTAL_COUNT_HEADER_NAME,
  NEXT_PAGE_HEADER_NAME,
  PAGE_HEADER_NAME,
  PER_PAGE_HEADER_NAME,
  PAGES_COUNT_HEADER_NAME,
} from './constants';
import * as express from 'express';

export const initPaginationMeta = (): IPaginationMeta => {
  return {
    limit: 50,
    offset: 0,
  };
};

export const generatePaginationHeader = (paginatinatedResult: IPagination<any>) => {
  const pageNo =
    (paginatinatedResult.offset + paginatinatedResult.limit) / paginatinatedResult.limit;
  const totalPages = Math.ceil(paginatinatedResult.total / paginatinatedResult.limit);
  return {
    [TOTAL_COUNT_HEADER_NAME]: paginatinatedResult.total,
    [NEXT_PAGE_HEADER_NAME]: pageNo + 1,
    [PAGE_HEADER_NAME]: pageNo,
    [PAGES_COUNT_HEADER_NAME]: totalPages,
    [PER_PAGE_HEADER_NAME]: paginatinatedResult.limit,
  };
};

export const populatePaginationHeaders = <T>(result: IPagination<T>, res: express.Response) => {
  const paginationHeaders = generatePaginationHeader(result);
  for (const header in paginationHeaders) {
    if (paginationHeaders[header]) {
      res.setHeader(header, paginationHeaders[header]);
    }
  }
};

export const generatePaginationHeaderV2 = (paginatinatedResult: BaseResponse<any>) => {
  return {
    [TOTAL_COUNT_HEADER_NAME]: paginatinatedResult.totalDocs,
    [NEXT_PAGE_HEADER_NAME]: paginatinatedResult.nextPage,
    [PAGE_HEADER_NAME]: paginatinatedResult.page,
    [PAGES_COUNT_HEADER_NAME]: paginatinatedResult.totalPages,
    [PER_PAGE_HEADER_NAME]: paginatinatedResult.limit,
  };
};

export const generatePaginationMetadata = (paginatedResult: BaseResponse<any>) => {
  return {
    currentPage: paginatedResult.page,
    nextPage: paginatedResult.nextPage,
    pageSize: paginatedResult.limit,
    pageCount: paginatedResult.totalPages,
    totalCount: paginatedResult.totalDocs,
  };
};

export const paginationSpread = (paginatedResult: BaseResponse<any>) => {
  return _.omit(paginatedResult, [
    'docs',
    'totalDocs',
    'limit',
    'totalPages',
    'page',
    'pagingCounter',
    'hasPrevPage',
    'hasNextPage',
    'prevPage',
    'nextPage',
  ]);
};
