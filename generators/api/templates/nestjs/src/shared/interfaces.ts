export interface IPagination<T> extends IPaginationMeta {
  total: number;
  items: T[];
}

export interface IPaginationMeta {
  limit: number;
  offset: number;
}
