import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { IConfig } from 'config';
import * as express from 'express';
import * as helmet from 'helmet';
import * as httpContext from 'express-http-context';
import { createLightship } from 'lightship';
import { Logger } from 'nestjs-pino';
import * as responseTime from 'response-time';
import { v4 as uuidV4 } from 'uuid';
import { AppModule } from './modules/app/app.module';
import { CONFIG } from './modules/config/config.provider';
import { initializeSwagger } from './shared/swagger.helper';
import { CORS_EXPOSED_HEADERS } from './shared/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });
  const config = app.get<IConfig>(CONFIG);
  const logger = app.get(Logger);

  app.useLogger(logger);

  initializeApp(app);
  await initializeSwagger(app);

  const lightship = await initializeLightship(app);

  await app.listen(config.get<number>('server.port'));

  lightship.signalReady();
}

async function initializeApp(app: INestApplication) {
  const config = app.get<IConfig>(CONFIG);
  app.use(helmet());
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());
  app.enableCors({
    exposedHeaders: CORS_EXPOSED_HEADERS,
  });
  app.use(responseTime({ header: 'x-response-time' }));
  app.use((req: express.Request, res: express.Response, next: () => void) => {
    const correlationId = uuidV4();
    httpContext.set('timestamp', Date.now());
    httpContext.set('correlationId', correlationId);
    req['id'] = correlationId;
    next();
  });
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      validationError: {
        target: false,
        value: false,
      },
    }),
  );
  app.setGlobalPrefix(config.get('service.baseUrl'));
}

async function initializeLightship(app: INestApplication) {
  const lightship = createLightship();

  lightship.registerShutdownHandler(async () => {
    await app.close();
  });

  return lightship;
}

bootstrap();
