# <%= config.name %>


## Description

<Describe what this service does.>

## Installation

* `nvm install`
* `npm install`

<Mention any additional steps required.>

## Running the app

`npm run start:dev`
<Describe commands required to run the app in multiple environments.>

## Test

`npm test`
<Describe commands to run to test the service. Any additional manual test can be described here as well.>
