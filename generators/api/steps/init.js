const chalk = require('chalk');
const printMessage = require('print-message');

module.exports = {
  sayHello() {
    printMessage([`Let's generate a new ${chalk.bgCyan.white.bold('api')} service`]);
  },
};
