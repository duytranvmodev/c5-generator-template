const Generator = require('yeoman-generator');
require('lodash').extend(Generator.prototype, require('yeoman-generator/lib/actions/install'));
const chalk = require('chalk');

module.exports = function () {
  if (this.options['skip-install']) {
    this.log(
      chalk.green(`
        To install dependencies, run
        ${chalk.white('$')} cd ${this.answers.name}/
        ${chalk.white('$')} npm install
      `),
    );
    return;
  }

  const folderName = this.answers.name;
  this.npmInstall(null, { save: true }, { cwd: folderName });
};
