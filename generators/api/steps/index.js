const writing = require('./writing');
const prompting = require('./prompting');
const install = require('./install');
const init = require('./init');
const end = require('./end');

module.exports = {
  prompting,
  writing,
  install,
  init,
  end,
};
