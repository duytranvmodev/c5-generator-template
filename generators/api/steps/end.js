/**
 * Called last, cleanup, say good bye, etc
 */
const chalk = require('chalk');
const printMessage = require('print-message');

module.exports = function () {
  printMessage(
    [
      `Enjoy your ${chalk.red(`${this.answers.name} API`)} project!`,
      '---',
      'Next steps:',
      `${chalk.yellow('1)')} Go to your generated api`,
      chalk.blue('cd <path_to_your_generated_app>'),
      `${chalk.yellow('2)')} Run your api`,
      chalk.blue(`npm run start:dev`),
    ],
    {
      printFn: this.log,
    },
  );
};
