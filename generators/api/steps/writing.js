const path = require('path');

module.exports = function () {
  const { answers } = this;
  const payload = {
    config: answers,
  };
  if (answers.kindOfFramework === 'NestJs') {
    const fileOrDirectoryList = [
      'config',
      'src',
      'test',
      'package.json',
      '.eslintignore',
      '.eslintrc.js',
      '.gitignore',
      '.gitlab-ci.yml',
      '.nvmrc',
      '.prettierignore',
      '.prettierrc',
      'nest-cli.json',
      'nodemon.json',
      'nodemon-debug.json',
      'README.md',
      'swagger.json',
      'swagger-2.0.json',
      'tsconfig.json',
      'tsconfig.build.json',
      'Dockerfile',
      '.dockerignore',
    ];

    if (this.answers.isIncludeNewRelic === 'y') {
      fileOrDirectoryList.push('newrelic.js');
    }
    if (this.answers.isIncludesSonar === 'y') {
      fileOrDirectoryList.push('sonar-project.properties');
    }

    fileOrDirectoryList.map((fileOrDirectory) =>
      this.fs.copyTpl(
        this.templatePath(path.join(`./nestjs/${fileOrDirectory}`)),
        this.destinationPath(`${this.answers.name}/${fileOrDirectory}`),
        payload,
      ),
    );
  }
  if (answers.kindOfFramework === 'Strapi') {
    const fileOrDirectoryList = [
      '.husky',
      'docs',
      'eslint',
      'scripts',
      'strapi',
      '.dockerignore',
      '.eslintignore',
      '.gitignore',
      '.prettierignore',
      '.prettierrc',
      'docker-compose.yaml',
      'Dockerfile',
      'esbuild.register.js',
      'package.json',
      'README.md',
      'tsconfig.eslint.json',
      'tsconfig.json',
      'yarn.lock'
    ];

    fileOrDirectoryList.map((fileOrDirectory) =>
      this.fs.copyTpl(
        this.templatePath(path.join(`./strapi/${fileOrDirectory}`)),
        this.destinationPath(`${this.answers.name}/${fileOrDirectory}`),
        payload,
      ),
    );
  }
};
