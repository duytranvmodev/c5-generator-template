const chalk = require('chalk');
const questionsDictionary = require('../questions');

function askQuestions(title, questions, done) {
  this.log(chalk.yellow(`\n${title} question${questions.length > 1 ? 's' : ''}:`));

  return this.prompt(questions).then((answers) => {
    this.answers = Object.assign(this.answers || {}, answers);
    done();
  });
}

module.exports = {
  askQuestions() {
    askQuestions.call(this, 'Application', questionsDictionary.app, this.async());
  },
};
