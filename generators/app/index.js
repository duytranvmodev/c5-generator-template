'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const printMessage = require('print-message');

module.exports = class extends Generator {
  prompting() {
    // eslint-disable-next-line no-warning-comments
    console.log(`${chalk.yellow(`
      ░█████╗░███████╗ ⏤͟͟͞͞★ ⏤͟͟͞͞★ ⏤͟͟͞͞★ ⏤͟͟͞͞★ ⏤͟͟͞͞★
      ██╔══██╗██╔════╝
      ██║░░╚═╝██████╗░
      ██║░░██╗╚════██╗
      ╚█████╔╝██████╔╝
      ░╚════╝░╚═════╝░
    `)}`);
    printMessage([
      `Welcome to the
       ${chalk.bgCyan.white.bold('C5-boilerplate generator!')}
       Let's generate a new app`,
    ]);

    const prompts = [
      {
        type: 'list',
        name: 'projectType',
        message: 'Choose type of project you want to generate',
        default: 'api',
        choices: ['api', 'NOPE NOPE NOPE -- quit!'],
      },
    ];

    return this.prompt(prompts).then((answers) => {
      this.answers = answers;
    });
  }

  end() {
    if (this.answers.projectType === 'api') {
      this.composeWith('c5-template:api', {
        answers: this.answers,
      });
    }
  }
};
