import * as fs from 'fs';

import { schedule, danger, warn, markdown, fail } from 'danger';
import noTestShortcuts from 'danger-plugin-no-test-shortcuts';

schedule(async () => {
  // Setup
  const mr = danger.gitlab.mr;

  // Check for .skip or .only in tests
  await noTestShortcuts({
    testFilePredicate: (filePath) => filePath.endsWith('.spec.js'),
    skippedTests: 'fail',
  });

  // Check if MR is BIG!
  const BIG_MR_THRESHOLD = 200;
  const linesOfCode = await danger.git.linesOfCode();
  if (linesOfCode > BIG_MR_THRESHOLD) {
    warn(`:exclamation: Big MR`);
    markdown(
      `> (${linesOfCode} (actual) > ${BIG_MR_THRESHOLD} (expected)) Merge Request size seems relatively large. If Merge Request contains multiple changes, split each into separate MR will helps faster, easier review.`,
    );
  }

  // Check for description
  if (mr.description.length < 10) {
    fail('This merge request needs a description.');
  }

  // Gather changes
  const modifiedFiles = danger.git.modified_files.filter(
    (path) => path.endsWith('js') && !path.endsWith('.spec.js'),
  );

  // Check for console.log statements
  modifiedFiles.forEach((file) => {
    const content = fs.readFileSync(file).toString();
    if (content.includes('console.log') || content.includes('console.warn')) {
      fail(`a \`console.log\` was left in file (${file})`);
    }
  });
});
